require 'STAT/STATstatus'

local function ReadFile(FileName)
   local F = io.open(FileName,'r')
	local C = F:read("*a")
   F:close()
   return C
end

function STATstatusBlock(Match, Polltime, FileName)
   local R =''
   R = R .. STATrow("Watching for", Match)
   if (FileName) then
      R = R..STATrow("Last file processed", FileName)
   end
   R = R .. STATrow("Running next at", os.date("%H:%M:%S", os.time()+Polltime));
   return R
end

function main(Data)
   component.setBackground{color="40FF00"}
   local Dir =component.fields().Home
   local Match = Dir..component.fields().Match
   local Polltime = component.fields()["Poll"] or 10;
   iguana.log("Polling every "..Polltime.." seconds");
   trace(Polltime)
   if not os.fs.access(Dir) then
      component.setStatus{data=STATerrorBlock("Directory "..
                        Dir.. " does not exist.")}
   else
      local LastFile
      for FileName in os.fs.glob(Match) do
         local Out = ReadFile(FileName)
         iguana.log("Read "..FileName.." ("..#Out.." bytes)");
         iguana.log(Out);
         queue.push{data=Out}
         LastFile = FileName
         if component.live() then os.remove(FileName) end                        
      end
      component.setStatus{data=STATstatusBlock(Match, Polltime, LastFile)}
   end  
   component.setTimer{delay=Polltime*1000, data=""}
end

-- This is how we prime the component to start
component.setTimer{delay=0, data=""} 